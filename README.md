
## Contents

This package contains the code and data used to generate the results in the following manuscript. 

A. J. Chowdhury, W. Yang, K. E. Abdelfatah, M. Zare, A. Heyden, and G. A. Terejanu, "A multiple filter based neural network approach to the extrapolation of adsorption energies on metal surfaces for catalysis applications" Journal of chemical theory and computation, vol. 16, iss. 2, pp. 1105-1114, 2020. doi:10.1021/acs.jctc.9b00986

## Author

This code has been written by *Asif Chowdhury* (asifc@email.sc.edu), while a PhD candidate at University of South Carolina.

## Instructions to run the code

To run the model, please run the 'ModelTester.py'.

In the last line of the file, there is a call to the method 'RunForFiltersAndRuns'. Parameters that needs to be tuned for specific problems:

```
	baseDir: it is the directory where the input output files are located.
	numOfFP: number of fingerprints used for each atom. For the sample input files (inside the 'Extrapolation' subfolder), this value is 8.
	hiddlayers: structure of the hidden layers in the atomic subnets. For example: if we set this value of [10,6], then there will be two hidden layers with 10 and 6 hidden units.
	testSetSize: the number of samples set aside as test set.
	filterRange: the range of filters the model will run for. This is zero-indexed and the range works as it is used in python. So, a value of (8,16) will run the model for
		     filter numbers 9, 10, 11, ..., 15, 16. Alternatively, you can also pass a list of filter numbers (zero-indexed). For example, a value of [3,5,6] will run
		     the model for filter numbers 4, 6, and 7.
	runsForFilter: For each filter, the number of times the data will be permuted and predictions made. For example, a value of 10 will ensure that the process of 'permuting the 
		       data set, divide into training, validation and testing, train the model and then make predictions on testing set' will happend 10 times to get an unbiased
		       estimate of the error. Initially, to see if a model is any good, you can set it to 1. For a promising model, increase this value to get a good estimate of error.
	runsForEnsemble: For each run of the model, we actually run the model this number of times to get ensemble estimate of the prediction. Initially, during investigation, you can
                         set this to a small value, say 3, and when you get promising results from a model, increase it to upto 8 or 10 to get potentially better results.
```

Inside the method 'RunForFiltersAndRuns', there is initialization of the object 'SubnetWrapper'. Set these relevant parameters:

```
	maxC:  maximum number of carbon atoms in a species in the dataset.
	maxO:  maximum number of oxygen atoms in a species in the dataset.
	maxH:  maximum number of hydrogen atoms in a species in the dataset. If all species contains only C, O, and H, then set this to zero since the connectivity information from
               C and O fully describe the species. The input encoding files should have exactly 'numOfFP * (maxC + maxO + maxH)' number of columns.
	The rest of the parameters are best left untouched for now. If you have a good understanding of the model, then you can tune these params.
```

There is also a helper function named 'readFiles' that is called to build the numpy arrays for inputs to the model. This function takes the location of the encoding file, energy file, range of columns to use from each, and a list of species (if any) that you would want to omit from your predictive study.

### Structure of the input files:

For an example of the encoding file, take the file named 'PAC_8_8_fingerprints_for_subnet_sorted.csv' inside the 'Extrapolation' subfolder. The initial couple of columns show the 
chemical formula and the SMILES notation of the species. Then starts the columns that are actually used to run the model. There are 64 of these columns here since our numOfFP 
(number of fingerprints) is 8 which means each atom has a 8-length fingerprint, and there are max 8 atoms (since maxC = 4, maxO = 4, maxH = 0, so we have 4 + 4 + 0 = 8 max atoms). 
So, 8 atoms each having 8 fingerprints, that makes 64 columns. For more detailed understanding of the fingerprints and the model, please refer to the manuscript and the SI:
\Heyden Group Team Folder\Asif\Extrapolation_JCTC


For an example of the energy file, take the file named 'PAC_energies.csv' inside the 'Extrapolation' subfolder. The first few columns you can ignore. The only important column
is the last one: 'energy'. 


## License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

## Acknowledgment
This material is based upon work supported by the National Science Foundation
under Grant No. DMREF-1534260.